const Emitter = require( 'events' );
const WorkItem = require( './workItem' );
const TextProps = require( './workDef' ).TextProps;

/**
 * Parses WorkDefs and computes values of WorkItems.
 * Emits Events:
 * itemCreated(WorkItem)
 * itemDone(WorkItem)
 * itemWillMerge(WorkItem, object)
 */
module.exports = class Runner extends Emitter {

	/**
	 * @property {Object.<string,WorkDef>}
	 */
	get workDefs() { return this._workDefs; }
	set workDefs(v) { this._workDefs = v; }

	/**
	 * Constructor for a Runner which creates WorkItems from the WorkDefs provided.
	 * @param {?Object.<string,WorkDef>} [defs=null] - collection of WorkDefs
	 */
	constructor( defs=null ) {

		super();

		this.workDefs = defs;

	}

	/**
	 * 
	 * @param {(string|WorkDef)} workDef - Work definition to run.
	 * @param {Object} [params={}] - Parameters passed to the root WorkItem object.
	 * @param {Object} [globals={}] - Global variables passed to all WorkItems.
	 * @returns {?WorkItem} - The completed WorkItem, or null if there was an error
	 * creating the WorkItem.
	 */
	run( workDef, params={}, globals={} ) {

		if ( typeof(workDef )==='string') workDef = this._workDefs[workDef];

		return this.makeItem( workDef, params, globals );

	}

	/**
	 * 
	 * @param {WorkDef} workDef - Defintion for constructing the WorkItem.
	 * @param {Object} [props=null] key/value properties to pass to the WorkItem.
	 * @param {Object} [globals=null] global variables to pass to the WorkItem and
	 * its descendents.
	 * @returns {?WorkItem} The created WorkItem, or null if there was an error.
	 */
	makeItem( workDef, props, globals=null ) {

		if ( !workDef ) return null;

		let workItem = new WorkItem( workDef, props, globals );

		this.emit( 'itemCreated', workItem );

		this.makeSubs( workItem );

		// assign any text-replace variables.
		for( let p of TextProps ) {
			if ( workDef[p]) workItem[p] = workItem.makeText( workDef[p]);
		}

		if (!props||!props.merge ){
			this.emit( 'itemDone', workItem );
		}

		return workItem;

	}

	/**
	 * Create and process all subItems of a WorkItem.
	 * @param {WorkItem} workItem - The WorkItem being processed.
	 */
	makeSubs( workItem ) {

		let subParams = workItem.subDefs;
		if ( Array.isArray(subParams) ) {

			let len = subParams.length;
			for( let i = 0; i < len; i++ ) {

				this.makeSub( workItem, subParams[i]);

			}

		} else if ( subParams instanceof Object ){

			 this.makeSub( workItem, subParams );

		}

	}

	/**
	 * Create a sub WorkItem of a parent WorkItem, based on the given parameters and properties.
	 * @param {WorkItem} workItem - parent WorkItem.
	 * @param {(string|Object)} subParams - Information on how the child WorkItem is created.
	 * These are parameters defined by the parent workItem.
	 * If no "use" property is specified, or if the use-string doesn't match a WorkDef,
	 * the params themselves are used as the child work object.
	 * @param {?Object} extraProps - additional props to set on the child WorkItem.
	 * @returns {(?Object|WorkItem)}
	 */
	makeSub( workItem, subParams, extraProps ) {

		var sub, workDef;

		// subItem is just a display string.
		if ( typeof(subParams) === 'string') {
	
			workDef = this.workDefs[subParams];
			if ( workDef ) sub = this.makeItem( workDef, {}, workItem.globals );
			else sub = this.makeSubObject( workItem, subParams );
			workItem.addChild(sub);

		} else {

			// params contain a requirement test before executing.
			if ( subParams.require && !workItem.testValue( subParams.require ) ) return null;

			if ( subParams['foreach'] ) return this.doForEach( workItem, subParams );

			if ( subParams.use ) {

				workDef = this.workDefs[ workItem.getString( subParams.use ) ];

				if ( !workDef ) return console.warn('WorkDef not found: ' + workItem.getString(subParams.use) );

				extraProps = workItem.getSubProps(subParams.assign, extraProps );
				if ( subParams.merge ) extraProps.merge = subParams.merge;
				sub = this.makeItem( workDef,
					extraProps,
					workItem.globals );

			} else sub = this.makeSubObject( workItem, subParams );

			/**
			 * Back-store the workItem so it can be referenced by the parent item
			 * or the parent item's children.
			*/
			if ( subParams.store ) workItem.storeVar( subParams.store, sub );

			if ( subParams.merge ) {
				this.emit( 'itemWillMerge', workItem, sub );
				workItem.mergeItem( sub );
				return null;
			} else workItem.addChild(sub);

		}

		return sub;

	}

	/**
	 * Create a simple sub Item from Object only;
	 * not a WorkItem with a WorkDef.
	 * @param {WorkItem} parent WorkItem
	 * @param {Object} obj - object describing the construction of the subItem.
	 * @returns {string|Object} The object created.
	 */
	makeSubObject( parent, obj ) {

		if ( typeof(obj) === 'string' ) {

			return parent.makeText( obj );

		} else if ( obj instanceof Object ) {

			let res = {};
			for( let p in obj ) {

				if ( TextProps.includes(p) ) res[p] = parent.makeText( obj[p]);
				else res[p] = parent.getValue( obj[p] );

			}

			return res;
		}

	}

	/**
	 * Generate a list of sub-work items based on an object describing a loop.
	 * @param {WorkItem} workItem -the parent WorkItem.
	 * @param {Object} subParams - object describing the subItem parameters and foreach loop.
	 * @returns {?Object[]}
	*/
	doForEach( workItem, subParams ) {

		let loopParams = subParams['foreach'];
		if ( typeof loopParams !== 'object' ) return null;

		// foreach[in] must give the prop-path to an Array variable in the WorkItem.
		// @todo - Allow all iterables.
		let arr = workItem.getValue( loopParams['in'] );
		if ( !Array.isArray(arr) ) {
			console.error( `Array expected: ${loopParams['in']} \nGot: ${arr}` );
			return null;
		}

		/**
		 * name of variable in child worItem to assign the iterated variable.
		 */
		let assignProp = loopParams['var'] || null;

		var subItem, subprops, subs = [];
		let len = arr.length;

		for( let i = 0; i < len; i++ ) {

			// assign current array value to matching property.
			subprops = assignProp ? { [assignProp]:arr[i] } : {};

			// loopParams are used as params to avoid infinite recursion.
			subItem = this.makeSub( workItem, loopParams, subprops );
			if ( subItem ) subs.push( subItem );

		}

		return subs;

	}

}