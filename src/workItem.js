const Formula = require('./formulas').Formula;
const VarPath = require('./varPath');

/**
 * @constant {RegExp} varRegEx - finds ${var_name} variables to replace
 * within text strings.
 */
const varRegEx = /\$\{(\w+(?:\.\w+)*)\}/g;

/**
 * Uses a WorkDef or generic Object to compute concrete
 * properties, formulas, and sub workItems based on the
 * WorkDef template.
 * Many options exist for the construction of WorkItems from templates.
 */
module.exports = class WorkItem {

	/**
	 * @property {WorkDef} workDef - Structure of the WorkItem.
	 * Can be defined in json format.
	 */
	get workDef() { return this._def; }
	set workDef(v) { this._def = v;}

	/**
	 * @property {Object[]} subDefs - Information about sub-work items from
	 * the workDef object.
	 */
	get subDefs() { return this._def.items; }

	/**
	 * @property {Object.<string,*>} vars - Variables computed from props or set by the parent WorkItem.
	 */
	get vars() { return this._vars; }
	set vars(v) { this._vars = v; }

	/**
	 * @property {?Object[]} children - child items of this WorkItem.
	 */
	get children() { return this._children; }
	set children(v) { this._children = v; }

	/**
	 * @property {*} result - optional result of the WorkItem. This can be set by a formula,
	 * or in a 'workDone' event handler.
	 */
	get result() { return this._result; }
	set result(v) { this._result = v; }

	/**
	 * 
	 * @param {WorkDef} workDef - definition for the content of the workItem.
	 * @param {?Object} [props=null] Starting properties to assign to variables. 
	 * @param {?Object} [globals=null] Global variables for WorkItem.
	 */
	constructor( workDef, props=null, globals=null ) {

		this._def = workDef;

		this.vars = props ? Object.assign( {}, props ) : {};

		this.globals = Object.assign( {}, globals );

		Object.assign( this.vars, this.globals );

		this.copyDef( this._def );

		// only need to compute variables that come from definition.
		if ( this._def.vars ) this.computeVars( this._def.vars );

		/**
		* Add any global variables defined in the work definition.
		* These additional globals will be passed on to child items.
		*/
		if ( this._def.globals ) {

			if ( typeof(this._def.globals) === 'string') this.globals[ this._def.globals ] = this.vars[ this._def.globals ];
			else for( let p of this._def.globals ) {
				//console.log( 'setting global: ' + p + '  -> ' + this.vars[p] );
				this.globals[p] = this.vars[p];
			}

		}
	
	}

	/**
	 * Copy any unknown/custom items from definition to this workItem.
	 * These could be program-specific formulas, etc. not defined in vars.
	 * @param {WorkDef|Object} def - WorkItem definition.
	 */
	copyDef( def ) {

		let proto = Object.getPrototypeOf( this );
		for( let p in def ) {

			if ( p.charAt(0) === '_' ) continue;	// ignore private variables.
			if( !(this.hasOwnProperty(p) || proto.hasOwnProperty(p)) ) {
				this[p] = def[p];
			}

		}

	}

	/**
	 * Add one or more children to this WorkItem.
	 * @param {WorkItem|Array|Object|string} childItem 
	 */
	addChild( childItem ) {

		this._children = this._children || [];
		if ( Array.isArray(childItem) ) this._children = this._children.concat( childItem );
		else this._children.push( childItem );

	}

	/**
	 * Compute any variables defined by the variable source.
	 * These are in the form of "variableName":varValue,
	 * where varValue can be a constant, a formula, or a var-path to a value.
	 * @param {Object} varSrc
	 * @param {number} [max=2] maximum times to recurse this method in order
	 * to resolve references to uncomputed variables. TODO: replace with better method.
	 */
	computeVars( varSrc, max=2) {

		/**
		 * Each variable definition links a variable name to a var-path or formula
		 * from globals and the workItem's own properties.
		 * These variables can be referenced in other formulas.
		 */
		var uncomputed;

		for( let p in varSrc ) {

			var varData = this.getValue( varSrc[p] );
			if ( varData === undefined ) {

				if ( !uncomputed ) uncomputed = {};
				uncomputed[p] = varSrc[p];
				//console.log('uncomputed: ' + p);

			} else this._vars[p] = varData;

			//console.log('assign ' + p + ' --> ' + varData );
		}

		if ( uncomputed && --max > 0 ) this.computeVars( uncomputed, max );

	}

	/**
	 * Merge another item into this instance.
	 * WorkItem variables are merged, children arrays are concatenated,
	 * and result objects are merged, providing both are Objects.
	 * Certain keywords are excluded to prevent infinite loops and illogical use cases.
	 * @todo: expand on exclusion parameters.
	 * @param {Object|WorkItem} item - item to merge.
	*/
	mergeItem( item ) {

		this._vars = Object.assign( this._vars, item._vars );
		this.title = item.title;
		this.desc = item.desc;

		if ( item._children ) {
			this._children = this._children ? this._children.concat( item._children) : item._children;
		}

	}

	/**
	 * Call a function at the given variable path, and return the result.
	 * @param {string} funcPath - path to the function relative to the globals
	 * or vars of this WorkItem.
	 * @param {?Array} [values=null] Array of params to pass to function. 
	 * @returns {*} The return value of the function, or undefined if function
	 * does not exist.
	 */
	callFunc( funcPath, values ) {

		let f = funcPath instanceof VarPath ? funcPath.readVar( this._vars ) : this.readVar( funcPath );
		if ( !f || typeof(f) !== 'function' ) {
			console.warn('func not found: ' + funcPath );
			return undefined;
		}
		let vals;

		if ( values ) {

			vals = values.slice(0);
			for( let i = vals.length-1; i >= 0; i-- ) {
				vals[i] = this.readVar( vals[i]);
			}

		} else vals = null;

		return f.apply( this.readCaller(funcPath), vals );

	}

	/**
	 * Retrieve the caller object for a given function path.
	 * @param {string} funcPath - path to function.
	 * @returns {?Object} Object to use as 'this' within the function.
	 */
	readCaller( funcPath ) {

		if ( funcPath instanceof VarPath ) funcPath = funcPath.path;
		let ind = funcPath.lastIndexOf('.');

		if ( ind < 0 ) return null;

		return this.readVar( funcPath.slice(0, ind ));

	}

	/**
	 * Store a value in the WorkItem's variables object.
	 * @param {string|VarPath} varName - variable name.
	 * @param {*} varVal - variable value.
	 */
	storeVar( varName, varVal ) {
		if ( varName instanceof VarPath) this._vars[varName.path] = varVal;
		else this._vars[ varName ] = varVal;
	}

	/**
	 * Create props to pass to a sub Item.
	 * For every string->string k/v pair in assigns, the sub Item's property k is assigned
	 * the current value of the variable v of this WorkItem.
	 * If assigns is a string s, subprops[s] is assigned this.vars[s]
	 * @param {string|Object|VarPath} assigns - subItem assignments.
	 * @param {Object} [subprops=null] - additional properties to pass to the child WorkItem.
	 * @returns {Object} - properties to use to intialize the sub item.
	 */
	getSubProps( assigns, subprops=null ) {

		// allow assignments to search passed subprops for assignment values as well.
		// this allows prop assignments to access a loop assignment value from a forEach.
		var srcVars = subprops ? Object.assign( {}, this._vars, subprops ) : this._vars;
		subprops = subprops || {};

		if( assigns instanceof VarPath) {

			subprops[ assigns.path ] = assigns.readVar( srcVars );

		} else if ( typeof(assigns) === 'string' ) {

			// If assignment is a single string, the same name is used
			// for both the parent variable and child property.
			subprops[assigns] = this.getValue(assigns);

		} else if ( assigns instanceof Formula ) {

			assigns.eval( srcVars, srcVars );

		} else {

			/**
			 * Build props for subitem.
			 * params.assign = { propsName:"var path or formula to assign" }
			 */
			let assign;
			for( let p in assigns ) {

				assign = assigns[p];

				if ( assign instanceof VarPath) subprops[p] = assign.readVar( srcVars );
				else if ( assign instanceof Formula ) subprops[p] = assign.eval( srcVars, subprops );
				else subprops[p] = this.getValue( assign );

			}

		}

		return subprops;

	}

	/**
	 * Test a true/false value on this WorkItem.
	 * Arrays will evaluate to true iff every value in the array
	 * evaluates to true.
	 * @param {Array|Formula|string|Number|boolean} obj - object to test.
	 * @returns {boolean} True if evaluated object is truthy, false otherwise.
	 */
	testValue( obj ) {

		if ( Array.isArray(obj) ) {

			for( let i = obj.length-1; i >= 0; i-- ) {
				if ( !this.testValue(obj[i])) return false;
			}
			return true;

		} else if ( obj instanceof Formula ) {

			return obj.eval( this._vars, this._vars ) == true;

		} else if (obj instanceof VarPath ) {

			return obj.readVar( this._vars ) == true;

		} else if ( typeof(obj) === 'string') {

			return this.readVar( obj ) == true;

		}

		return obj == true;

	}

	/**
	 * Use the WorkItem's variables to compute a value.
	 * @param {*} obj - object to compute.
	 * @returns {*} The value computed.
	 */
	getValue( obj ) {

		if ( isNaN(obj) === false ) return obj;
		else if ( obj instanceof VarPath ) return obj.readVar( this._vars );
		else if ( obj instanceof Formula ) {

			return obj.eval( this._vars, this._vars );

		} else if ( obj instanceof Object ) {

			if ( obj.hasOwnProperty('if') === true ) {

				// if 'true' or 'false' results are omitted, variable is not assigned.
				// this allows earlier values, or earlier test if-cases to remain.
				if ( this.getValue( obj['if'] ) ) {

					if ( obj.hasOwnProperty('true') === true) return this.getValue( obj['true']);

				} else {

					if ( obj.hasOwnProperty('false') === true ) return this.getValue( obj['false']);

				}

			} else if ( obj.hasOwnProperty('switch') === true ) {

				// switch test assignment.
				let test = this.getValue( obj['switch']);
				if ( obj.hasOwnProperty(test) === true ) return this.getValue( obj[ test ] );
				else if ( obj.hasOwnProperty('default') === true ) return this.getValue( obj['default']);

			} else if ( obj['call']) return this.callFunc( obj['call'], obj['params']);

		}

		return obj;

	}

	/**
	 * Return object as a string value.
	 * Variable paths are not read as vars.
	 * 'Switch' and 'if' objects are evaluated but return results as strings.
	 * Formulas and function calls are also evaluated.
	 * @param {*} obj
	 * @returns {*} result string, or original object if no conversion
	 * is possible.
	 */
	getString( obj ) {

		if ( typeof obj === 'string' ) return obj;
		else if ( isNaN(obj) === false || obj instanceof VarPath ) return obj.toString();
		else if ( obj instanceof Formula ) {

			return obj.eval( this._vars, this._vars );

		} else if ( obj instanceof Object ) {

			if ( obj.hasOwnProperty('if') === true ) {

				// if 'true' or 'false' results are omitted, variable is not assigned.
				// this allows earlier values, or earlier test if-cases to remain.
				if ( this.getValue( obj['if'] ) ) {

					if ( obj.hasOwnProperty('true') === true) return this.getString( obj['true'] );

				} else {

					if ( obj.hasOwnProperty('false') === true ) return this.getString( obj['false']);

				}

			} else if ( obj.hasOwnProperty('switch') === true ) {

				// switch test assignment.
				let test = this.getValue( obj['switch']);
				if ( obj.hasOwnProperty(test) === true ) return this.getString( obj[ test ] );
				else if ( obj.hasOwnProperty('default') === true ) return this.getString( obj['default']);
				else return test;

			} else if ( obj['call']) return this.callFunc( obj['call'], obj['params']);


		}

		return obj;

	}

	/**
	 * Reads a value from the WorkItem's variables object.
	 * @param {string} varPath - variable path.
	 * @returns {*} The value stored at the given path, or undefined if value not found.
	 */
	readVar( varPath ) {

		if ( varPath instanceof VarPath ) return varPath.readVar( this._vars );
		if ( !varPath ) return undefined;

		varPath = varPath.split('.');

		let len = varPath.length;
		let obj = this._vars;

		for (let i = 0; i < len; i++) {

			if (!(obj instanceof Object)) return undefined;
			obj = obj[varPath[i]];

		}

		return obj;

	}

	/**
	 * Returns a variable value as a float.
	 * @param {string} varPath - Path to the variable.
	 * @param {number} [defaultVal=0] - Value to return if the variable path does not exist.
	 * @returns {number} - Numeric value stored at the variable path, or defaultVal.
	 */
	readVarNum( varPath, defaultVal=0 ) {

		if ( varPath instanceof VarPath ) return varPath.readVarNum( this._vars, defaultVal);
		if ( !varPath ) return defaultVal;

		varPath = varPath.split('.');
		let len = varPath.length;

		let obj = this._vars;

		for (let i = 0; i < len; i++) {

			if (!(obj instanceof Object)) return defaultVal;
			obj = obj[varPath[i]];

		}

		return Number(obj);

	}

	/**
	 * Returns a prop or subvalue as an integer.
	 * @param {string} varPath - path to the variable.
	 * @returns {number} the variable value interpreted as an integer, or defaultVal
	 * if the variable could not be found.
	 */
	readVarInt( varPath, defaultVal=0) {
		
		if ( varPath instanceof VarPath ) return varPath.readVarInt( this._vars, defaultVal);
		if ( !varPath ) return defaultVal;

		varPath = varPath.split('.');
		let len = varPath.length;

		let obj = this._vars;

		for (let i = 0; i < len; i++) {

			if (!(obj instanceof Object)) return defaultVal;
			obj = obj[varPath[i]];

		}

		return Number.parseInt(obj);

	}

	/**
	 * Compute a display string from a template string.
	 * @param {string} str - template string.
	 * @returns {string} string produced after variable substition.
	 */
	makeText( str ) {

		if ( !str ) return '';
		if ( typeof(str) !== 'string' ) {
			console.error('err not string: ' + str );
			return '';
		}

		return str.replace(
			varRegEx, (match, path)=>{
				//console.log('regex match: ' + match );
				//console.log('regex group: ' + path );
				return this.readVar( path );
			}
		);

	}

}