const util = require( './jsutil' );
const formulas = require( './formulas');
const Formula = formulas.Formula;
const VarPath = require('./varPath');

/**
 * @property {string[]} Object properties to treat as variable-replace text strings.
 */
const TextProps = ['name', 'title','desc'];

/**
 * @property {RegExp} StrRegEx - tests string for single string quotes.
 */
const StrRegEx = /^'[^']*'$/;

exports.TextProps = TextProps;

/**
 * Defines variables, formulas, and subItems for a single Item of work.
 * WorkDefs are used as templates to create actual WorkItems based on
 * program data.
 */
exports.WorkDef = class WorkDef {

	toJSON() { return util.jsonify( this ); }

	/**
	 * @property {string[]} Array of named parameters which should be supplied
	 * to the WorkItem from the parent item.
	 */
	get props() { return this._props; }
	set props(v) { this._props = v; }

	/**
	 * @property {string} id of the WorkDef.
	 */
	get id() { return this._id; }
	set id(v) { this._id = v; }

	/**
	 *
	 */
	get vars() { return this._vars; }
	set vars(v) {
		this._vars = this.parseValue(v);
	}

	/**
	 * @property {string} optional title of the workItem.
	 */
	get title() { return this._title;}
	set title(v) { this._title = v; }

	/**
	 * @property {string} optional description displayed for the WorkItem, with optional ${var} markers.
	 */
	get desc() { return this._desc; }
	set desc(v) { this._desc = v; }

	/**
	 * @property {string|string[]} Properties or formulas on the parent WorkItem that must evaluate to true
	 * in order to instantiate this WorkDef as a WorkItem.
	 */
	get require() { return this._require; }
	set require(v) { this._require = this.parseValue(v); }

	/**
	 * @property {string[]} - Skip this work item if any of these properties
	 * of the parent item are true.
	 */
	get forbid() { return this._forbid; }
	set forbid(v) { this._forbid = v; }

	/**
	 * @property {string|string[]} names of properties to set on all sub-items.
	 */
	get globals() {return this._globals; }
	set globals(v) { this._globals = v; }

	/**
	 * @property {object{}} - sub workItems to compute for this work item.
	 */
	get items() { return this._items; }
	set items(v) {
		this._items = this.parseValue(v);
	}

	constructor( json=null ) {

		let myProto = Object.getPrototypeOf( this );
		for( let p in json ) {

			if ( myProto.hasOwnProperty(p) === true ) this[p] = json[p];
			else this[p] = this.parseValue( json[p] );

		}

	}

	/**
	 * Convert a value that is used as a child-item parameter.
	 * @param {Array|string|Formula|object} v - raw value to parse.
	 * @returns {*} Value converted to a Number, Formula, string, Array or Object,
	 * as appropriate.
	 */
	parseValue(v) {

		/**
		* convert any formula strings in cost value to Formula objects.
		*/
		if ( isNaN(v) === false ) return v;
		else if ( typeof(v) === 'string') {

			if ( StrRegEx.test(v) === true ) return v.slice(1, v.length-1 );
			if ( VarPath.isPathString(v) === true ) v = new VarPath(v);
			else if ( formulas.containsOp(v) ) {

				let f = Formula.TryParse( v );
				if ( !f.error ) v = f;

			}

		} else if (Array.isArray(v) ) {

			let len = v.length;
			for( let i = 0; i < len; i++ ) {

				// strings on their own in an array remain unparsed.
				if ( typeof v[i] === 'string') continue;
				v[i] = this.parseValue( v[i]);

			}

		} else if ( v instanceof Object ) {

			for( let p in v ) {
				if ( TextProps.includes(p)) v[p] = this.parseString( v[p] );
				else v[p] = this.parseValue( v[p] );
				//console.log('parse def prop: ' + p + ' ' + v[p].toString() );

			}

		}

		//console.log('parsing def var: ' + v );

		return v;

	}

	/**
	 * Parse an object that should ultimately return a string variable,
	 * instead of a VarPath or formula.
	 * @param {*} v
	 */
	parseString( v ) {

		if ( isNaN(v) === false ) return Number(v);
		else if ( typeof v === 'string' ) return v;
		else if ( Array.isArray(v) ){

			let len = v.length;
			for( let i = 0; i < len; i++ ) {

				// strings on their own in an array remain unparsed.
				if ( typeof v[i] === 'string') continue;
				v[i] = this.parseString( v[i]);

			}

		} else if ( v instanceof Object ){

			for( let p in v ) {

				if ( p === 'switch' || p === 'if' ) v[p] = this.parseValue( v[p]);
				else v[p] = this.parseString( v[p] );

			}

		}

		return v;

	}

}