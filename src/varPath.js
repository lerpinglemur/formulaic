/**
 * @constant {RegExp} pathRegEx - determines if a string is a valid variable path.
 */
const pathRegEx = /^[A-Za-z_]+(?:\.\w+)*$/;

module.exports = class VarPath {

	/**
	 * Determines if a string represents a variable path.
	 * @param {string} s string to test.
	 * @returns {bool} whether the string could represent a variable path. 
	 */
	static isPathString( s ) {
		return pathRegEx.test(s);
	}

	/**
	 * @property {string[]} parts - parts of the variable path.
	 */
	get parts() { return this._parts; }
	set parts(v) {
		if ( Array.isArray(v) ) this._parts = [];
		else this._parts = v;
	}

	/**
	 * @property {string} path - The variable path string.
	 */
	get path() { return this._parts ? this._parts.join('.') : '';}
	set path(v) {
		if ( typeof v === 'string' ) this._parts = v.split('.');
		else if ( Array.isArray(v) ) this.parts = v;
		else this._parts = [];
	}

	/**
	 * 
	 * @param {string} s - path to a variable. 
	 */
	constructor(s) {

		if ( typeof s === 'string' ) this.path = s;
		else if ( Array.isArray(s) ) this._parts = s;
		else this._parts = [];

	}

	/**
	 * @returns {string} The variable path as a string.
	 */
	toString() { return this.path; }

	/**
	 * Read the value stored in an object at this variable path.
	 * @param {Object} - base object of the path.
	 * @returns {*} The value stored at the variable path, or defaultVal.
	 */
	readVar( obj ) {

		let varPath = this._parts;

		let len = varPath.length;

		for (let i = 0; i < len; i++) {

			if ( typeof obj !== 'object' ) return undefined;
			obj = obj[varPath[i]];

		}

		return obj;

	}

	/**
	 * Returns a property (or sub) value as a float.
	 * @param {number} [defaultVal=0] - Value to return on variable not found.
	 * @returns {number|undefined|NaN} - Numeric value stored at the variable path, or defaultVal.
	 */
	readVarNum( obj, defaultVal=0 ) {

		let varPath = this._parts;
		let len = varPath.length;

		for (let i = 0; i < len; i++) {

			if ( typeof obj !== 'object' ) return defaultVal;
			obj = obj[varPath[i]];

		}

		return Number(obj);

	}

	/**
	 * Returns a prop or sub value as an integer.
	 * @param {Object} obj - base variable of the path.
	 * @param {number} [defaultVal=0] - value to return on variable not found.
	 * @returns {number|undefined} the variable value interpreted as an integer, or defaultVal
	 * if the variable could not be found.
	 */
	readVarInt( obj, defaultVal=0) {

		let varPath = this._parts;
		let len = varPath.length;

		for (let i = 0; i < len; i++) {

			if ( typeof obj !== 'object' ) return defaultVal;
			obj = obj[varPath[i]];

		}

		return Number.parseInt(obj);

	}

}